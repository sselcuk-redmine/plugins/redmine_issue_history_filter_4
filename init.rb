require_dependency 'redmine_issue_history_filter_hook_listener'

Redmine::Plugin.register :redmine_issue_history_filter_4 do
  name 'Issue History Filter Plugin 4'
  author 'Serkan Selcuk'
  description 'This plugin provides simple filters for the issue history block and shows comments only as default.'
  version 'v04.00'
  url 'https://gitlab.com/sselcuk-redmine/plugins/redmine_issue_history_filter_4'
end
